//
//  TableViewController.swift
//  Homework 1
//
//  Created by Haavri on 06/05/2017.
//  Copyright © 2017 Haavri. All rights reserved.
//

import UIKit


/*
 
 Tema : Un UIViewController cu tableView in  el  + doua butoane in care
 1) - daca apesi pe unu se adauga o celula
 - pe celalat se sterge ultima celula
 
 2) - daca apesi pe o celula sa te duci la urmatorul ecran (vezi data trecuta)
 
 3) - la urmatorul ecran sa iti afiseze diferit in functie de ce celula ai apasat
 (sa pasati date de la un screen la altul)
 
 4) -  UITableViewDelegate
 
 optional public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? // custom view for header. will be adjusted to default or specified header height
 
 @available(iOS 2.0, *)
 optional public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? // custom view for footer. will be adjusted to default or specified footer height
 
 
 !!!!! Daca le implementati pe cele 2 vedeti ce se intampla cu  ->
 
 
 @available(iOS 2.0, *)
 optional public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? // fixed font style. use custom view (UILabel) if you want something different
 
 @available(iOS 2.0, *)
 optional public func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String?
 
 5) - swipe ca sa stergi celula cu buton de delete
 6) super extra bonus - custom cell din NIB/Storyboard
 
*/

class TableViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    
    let cellIdentifier = "identifier"
    
    var arrayOfValues = ["Apple","Samsung","Microsoft","HP","LG","ASUS","Tesla","Ford","Audi"]
    let arrayOfImages: [UIImage] = [#imageLiteral(resourceName: "sun"),#imageLiteral(resourceName: "cloud")]
    
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: cellIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayOfValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        cell.textLabel?.text = arrayOfValues[indexPath.row]
        
        return cell
    }
    
    
    // 1) - daca apesi pe un buton se sterge ultima celula
    
    @IBAction func deleteLastCell(_ sender: UIButton) {
        
        arrayOfValues.remove(at: arrayOfValues.count-1)
        //tableView.deleteRows(at: [IndexPath(row: arrayOfValues.count-1, section: 0)], with: .automatic)
        tableView.reloadData()
  
    }
    
    // 1) - daca apesi pe un buton se adauga o celula
    
    @IBAction func addCell(_ sender: UIButton) {
        
        arrayOfValues.append(textField.text!)
        //tableView.insertRows(at: [IndexPath(row: arrayOfValues.count-1, section: 0)], with: .automatic)
        tableView.reloadData()
        
    }
    
    
    /* 2) - daca apesi pe o celula sa te duci la urmatorul ecran (vezi data trecuta)
    
       3) - la urmatorul ecran sa iti afiseze diferit in functie de ce celula ai apasat
            (sa pasati date de la un screen la altul) */
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toSecondViewController", sender: arrayOfValues[indexPath.row])
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let cell = segue.destination as! SecondViewController
        cell.cellText = sender as! String
    }
    
    
    // 4) -  UITableViewDelegate

    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.orange
        
        let image = UIImageView(image: arrayOfImages[section])
        image.frame = CGRect(x: 5, y: 5, width: 35, height: 35)
        headerView.addSubview(image)
        
        let label = UILabel()
        label.text = "Header in section \(section)"
        label.frame = CGRect(x: 45, y: 5, width: 200, height: 35)
        headerView.addSubview(label)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        let footerView = UIView()
        footerView.backgroundColor = UIColor.green
        
        let label = UILabel()
        label.text = "Footer in section \(section)"
        label.frame = CGRect(x: 45, y: 5, width: 200, height: 35)
        footerView.addSubview(label)
        
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 45
    }

    
    /* public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return "Header in section \(section)"
    }
    
    public func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String?
    {
        return "Footer in section \(section)"
    } */
    
    
    // 5) - swipe ca sa stergi celula cu buton de delete
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete
        {
            print("sup dawg?")
            arrayOfValues.remove(at: indexPath.row)
            //tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.reloadData()
        }
    }
    
    
}
