//
//  SecondViewController.swift
//  Homework 1
//
//  Created by Haavri on 06/05/2017.
//  Copyright © 2017 Haavri. All rights reserved.
//

import UIKit

class SecondViewController : UIViewController
{
    
    @IBOutlet weak var textLabel: UILabel!
    
    var cellText = ""
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        textLabel.text = cellText
    }
    
}
